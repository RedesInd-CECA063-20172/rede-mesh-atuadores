#include <ESP8266WiFi.h>
#include <ESP8266WiFiAP.h>
#include <ESP8266WiFiGeneric.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WiFiScan.h>
#include <ESP8266WiFiSTA.h>
#include <ESP8266WiFiType.h>
#include <WiFiClient.h>
#include <WiFiClientSecure.h>
#include <WiFiServer.h>
#include <WiFiUdp.h>

#define pin_led 2

char* ssidap = "node2";
char* passap = "12345678";

String request="";
String command="";

IPAddress server(192,168,11,4);       // the fix IP address of the server
WiFiClient client;

void setup() {
  WiFi.disconnect();

  pinMode(pin_led, OUTPUT);
  digitalWrite(pin_led, LOW);
  
  Serial.begin(115200);

  WiFi.mode(WIFI_STA);

  WiFi.begin(ssidap, passap);             // connects to the WiFi router
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println("CONNECTED");    
}

void loop() {
  // put your main code here, to run repeatedly:
  if(Serial.available())
  {
    while(Serial.available()) 
    {
      command=Serial.readString();
    }
    
    if(command[0]=='1')
    {
      digitalWrite(pin_led,HIGH);
    }
    else
    {
      digitalWrite(pin_led,LOW);
    }

    if(command[1]=='1')
    {
      request="no2toggle";
    }
    else
    {
      request="no2untoggle";
    }
    client.connect(server, 80);   // Connection to the server
    client.println(request+"\r");  // sends the message to the server
    String answer = client.readStringUntil('\r');   // receives the answer from the sever
    client.flush();
    delay(100); 

    if(command[2]=='1')
    {
      request="no3toggle";
    }
    else
    {
      request="no3untoggle";
    }
    client.connect(server, 80);   // Connection to the server
    client.println(request+"\r");  // sends the message to the server
    answer = client.readStringUntil('\r');   // receives the answer from the sever
    client.flush();
    delay(100);
}
}
