#include <ESP8266WiFi.h>
#include <ESP8266WiFiAP.h>
#include <ESP8266WiFiGeneric.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WiFiScan.h>
#include <ESP8266WiFiSTA.h>
#include <ESP8266WiFiType.h>
#include <WiFiClient.h>
#include <WiFiClientSecure.h>
#include <WiFiServer.h>
#include <WiFiUdp.h>

#define pin_led 2

WiFiServer server(81); 

char* myssid = "node3";
char* mypass = "12345678";

IPAddress ip(192,168,12,4);
IPAddress gateway(192,168,12,1);
IPAddress subnet(255,255,255,0);

String request = "";

void setup() {
  // put your setup code here, to run once:
  pinMode(pin_led, OUTPUT);
  digitalWrite(pin_led, LOW);

  WiFi.mode(WIFI_AP);

  WiFi.softAPConfig(ip, gateway, subnet);
  WiFi.softAP(myssid, mypass);

  server.begin();

}

void loop() {
  Serial.println("Esperando Cliente");
  WiFiClient client = server.available();
  if (client) {
    if (client.connected()) {
      Serial.println(".");
      request = client.readStringUntil('\r');    // receives the message from the client
      
      Serial.print("From client: "); Serial.println(request);
      client.flush();
      client.println("ok\r");
      if (request=="no3toggle")
      {
        digitalWrite(pin_led,HIGH);
        request="";
      }  
      if (request=="no3untoggle")
      {
        digitalWrite(pin_led,LOW);
        request="";
      }  
    }
    client.stop();                // tarminates the connection with the client
  } 
}
