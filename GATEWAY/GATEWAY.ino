// Baseado no Software Modbus-Arduino Example - Lamp (Modbus Serial)
// de André Sarmento Barbosa (http://github.com/andresarmento/modbus-arduino)
 
#include <Modbus.h>
#include <ModbusSerial.h>

// Modbus Registers Offsets (0-9999)
const int LED_1 = 0; 
const int LED_2 = 1;
const int LED_3 = 2;
// Used Pins
const int ledPin = 13;

int led1buf, led2buf, led3buf, flagNewCom;

// ModbusSerial object
ModbusSerial mb;

String command = "";

void setup() {
    // Config Modbus Serial (port, speed, byte format) 
    mb.config(&Serial, 9600, SERIAL_8N1);
    // Set the Slave ID (1-247)
    mb.setSlaveId(1);  
    
    // Set ledPin mode
    pinMode(ledPin, OUTPUT);
    // Add LAMP1_COIL register - Use addCoil() for digital outputs
    mb.addCoil(LED_1);
    mb.addCoil(LED_2);
    mb.addCoil(LED_3);
    Serial3.begin(115200);
    led1buf=0;
    led2buf=0;
    led3buf=0;
    flagNewCom=0;
}

void loop() {
   // Call once inside loop() - all magic here
   mb.task();

   if((mb.Coil(LED_1)!=led1buf))
   {
    flagNewCom = 1;
   }
   else if((mb.Coil(LED_2)!=led2buf))
   {
    flagNewCom = 1;
   }
   else if((mb.Coil(LED_3)!=led3buf))
   {
    flagNewCom = 1;
   }

   

   if(flagNewCom==1)
   {
      led1buf=mb.Coil(LED_1);
      led2buf=mb.Coil(LED_2);
      led3buf=mb.Coil(LED_3);
      
      if(!mb.Coil(LED_1))
     {
      command = command+"1";
     }
     else
     {
      command = command+"0";
     }
     if(!mb.Coil(LED_2))
     {
      command = command+"1";
     }
     else
     {
      command = command+"0";
     }
     if(!mb.Coil(LED_3))
     {
      command = command+"1";
     }
     else
     {
      command = command+"0";
     }

     Serial3.print(command);

     boolean ledstate = !(command[0]-48);
     digitalWrite(ledPin,ledstate);

     command = "";

     flagNewCom=0;
   }  
   
   delay(10);
}
