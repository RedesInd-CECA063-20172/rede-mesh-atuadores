Contexto
========

Esse projeto foi apresentado como avaliação parcial para a aprovação na disciplina de Redes Industriais do curso de Engenharia de Controle e Automação do [Instituto Federal do Espírito Santo Campus Serra](http://www.ifes.edu.br).

Introdução
==========

O protocolo Modbus é uma estrutura de mensagens desenvolvida em 1979 pela empresa americana de instrumentos de controle e automação Modicon, hoje Schneider Electric ([Modbus Organization, [2017]](http://www.modbus.org/faq.php)). O protocolo consiste da definição de uma estrutura de mensagem que deve ser implementada em dispositivos inteligentes a fim de estabelecer uma conexão mestre/escravo ou cliente/servidor entre eles. Modbus é um protocolo totalmente aberto.

Segundo a [Modbus Organization ([2017])](http://www.modbus.org/faq.php), Modbus é o mais utilizado dos protocolos pertencentes ao ambiente industrial e de manufatura. A organização afirma que apenas na Europa e América do Norte, 7 milhões de dispositivos utilizam a solução.

Neste trabalho, uma rede Modbus serial foi empregada afim de proporcionar a comunicação entre três atuadores e um sistema ysupervisório.

A Rede Modbus Serial
====================

Uma rede Modbus serial possui dispositivos escravos que são comandados por um dispositivo mestre. Os dispositivos são conectados por um enlace serial apropriado.

Nesse tipo de rede, os escravos não são capazes de transmitir informações a menos que isso lhes seja ordenado pelo mestre ([ProSoft Technology, 2014](https://www.youtube.com/watch?v=k993tAFRLSE)). Cada mestre pode comportar 247 escravos e cada escravo é identificado por um id de 8 bits. Tipicamente os PLCs são os mestres das redes modbus, enquanto os equipamentos de campo desempenham o papel dos escravos ([ProSoft Technology, 2014](https://www.youtube.com/watch?v=k993tAFRLSE)).

O protocolo Modbus define um sistema de endereçamento e de tabelas de dados. As informações contidas nos instrumentos devem ser armazenadas em quatro diferentes tabelas: duas delas utilizadas para os dados discretos (coils) e duas delas para dados analógicos de até 16 bits (registers). Os dados contidos nas tabelas recebem endereços únicos que permitirão seu acesso. A tabela 1 apresenta as tabelas de dados implementadas pelo protocolo assim como o *range* de endereços disponíveis para os dados de cada tabela.

|        Tabela de Dados           |    Range de Endereços Disponíveis  |
|:--------------------------------:|------------------------------------|
|        Coil - ler/escrever       |            00001 a 09999           |
| Discrete Inputs - apenas leitura |            10001 a 19999           |
| Input Registers - apenas leitura |            30001 a 39999           |
| Holding Registers - ler/escrever |            40001 a 49999           |

Tabela 1 - Tabelas de Dados e Endereços Disponíveis Segundo o Protocolo Modbus

Fonte: [ProSoft Technology (2014)](https://www.youtube.com/watch?v=k993tAFRLSE)

Cada tabela possui 9999 endereços. Isso torna possível que cada instrumento modbus processe até 9999 dados de cada um dos quatro tipos especificados.

A fim de que o mestre possa enviar comandos aos escravos, o protocolo Modbus define também alguns códigos de função. A tabela 2 apresenta os códigos e as ações a serem tomadas pelos escravos ao receberem cada um deles.

| Código |               Ação              |
|:------:|:-------------------------------:|
|    1   |        Ler Status do Coil       |
|    2   |   Ler Status do Discrete Input  |
|    3   |  Ler Status do Holding Register |
|    4   |   Ler Status do Input Register  |
|    5   |    Escrever em um único Coil    |
|    6   |  Escrever em um único Register  |
|    7   |   Escrever em múltiplos Coils   |
|    8   | Escrever em múltiplos Registers |

Tabela 2 - Tabela de Códigos de Funções Segundo o Protocolo Modbus\
Fonte: [ProSoft Technology (2014)](https://www.youtube.com/watch?v=k993tAFRLSE)

A maneira como os dados são armazenados dentro de cada célula das tabelas não está definida no protocolo Modbus [@ProSoft]. Por isso é comum que, ao se utilizar instrumentos de diversos fabricantes, dados *Little Endian* se misturem a dados *Big Endian*. Deve-se ter cautela, portanto, no processamento das informações oriundas dos escravos.

A Rede Mesh de Atuadores
========================

A rede mesh de atuadores empregada consiste de três dispositivos NodeMCU DevKit conectados entre si por enlaces WiFi. Esses dispositivos constituem uma rede mesh uma vez que os pacotes que trafegam pela rede são encaminhados nó a nó até encontrarem seu destinatário.

O [NodeMCU](http://www.nodemcu.com/index_en.html) DevKit é uma placa de desenvolvimento de código aberto baseada no *Wi-Fi SoC* ESP8266 da Espressif Systems.

O protocolo de comunicação implementado na malha não segue nenhum padrão público. Na verdade um protocolo mínimo de funcionamento foi programado diretamente nos nós, permitindo-lhes identificar pacotes destinados a eles e a encaminhar para seu vizinho os pacotes que não lhe dizem respeito.

A simulação dos atuadores se dá pelo acionamento de LEDs presentes na própria placa.

Sistema Supervisório
====================

Como sistema supervisório, empregou-se neste projeto o software brasileiro, livre, gratuito e de código aberto [ScadaBR](http://www.scadabr.com.br/). O ScadaBR é um software de desenvolvimento de aplicações, aquisição de dados e controle supervisório para sistemas de automação e instrumentação. O sistema permite que o estado de transmissores, controladores e atuadores espalhados por toda uma planta seja supervisionado e controlado a distância por um computador.

Dentre outros protocolos, o ScadaBR entende Modbus, o que permitiu sua utilização como sistema supervisório neste projeto.

Gateway Modbus
==============

Uma vez que um protocolo particular foi utilizado na rede de atuadores, um gateway entre essa rede e a rede modbus se fez necessário. O objetivo foi a conexão da rede ao supervisório ScadaBR, que implementa Modbus.

Para tanto, a interface entre a rede mesh e a rede modbus foi feita através de um Arduino Mega. O [Arduino](https://www.arduino.cc/) Mega é uma placa de desenvolvimento de código aberto baseada nos microcontroladores ATMEGA da Atmel Corporation.

Para que o gateway fosse criado, programou-se o arduino de forma a fazê-lo entender tanto o protocolo da rede de atuadores quanto o protocolo modbus. Para isso, foram empregadas as biblitoecas Modbus.h e ModbusSerial.h.

O ScadaBR é agnóstico com relação ao protocolo empregado pelos atuadores. Uma vez que todo o fluxo de informações supervisório $\rightarrow$ rede passa pelo gateway, o arduino deve implementar as tabelas de dados e endereços do modbus afim de traduzir propriamente as informações que circulam na rede mesh para o protoclo Modbus
compreendido pelo supervisório.

Conclusão
=========

Através deste projeto foi possível verificar que a implementação do protocolo Modbus não requer grandes esforços de programação. Pelo contrário, com o uso de códigos simples e dispositivos de baixo custo, é possível supervisionar uma rede de atuadores.

O modbus *per se* não somente define a composição dos quadros de informação trocados pelos dispositivos, mas também o meio físico a ser empregado a fim de que a comunicação se estabeleça. Essa decisão, embora tenha proporcionado durante muitos anos um sistema robusto para comunicação industrial, tornou-se obsoleta com o avanço das tecnologias de comunicação. A fim de tornar o protocolo ainda mais acessível aos instrumentos de campo e “inserí-lo no século 21” ([Modbus Organization, [2017]](http://www.modbus.org/faq.php)), o padrão aberto Modbus TCP/IP foi desenvolvido em 1999. A pilha TCP/IP é um modelo conceitual e um conjunto de protocolos de comunicação entre máquinas desenvolvido pela Agência de Projetos de Pesquisa em Defesa Aplicada (DARPA), agência do Departamento de Defesa Norte-americano, nos anos de 1960 [@Cerf1983] e transformado em padrão global através do [RFC1122](https://tools.ietf.org/html/rfc1122.). A integração do protocolo Modbus com a pilha TCP/IP passou a permitir a utilização do padrão por virtualmente qualquer tipo de equipamento e dispositivo capaz de se conectar a uma rede de internet.

Embora neste trabalho tenha sido empregado o Modbus Serial, com alterações não muito custosas, o Modbus TCP/IP pode ser implementado, possibilitando que a rede seja integrada à internet, dispositivos de IoT, processamento em nuvem e todas as benesses dos avanços do setor de telecomunicações.

O projeto foi feito como prova de conceito e, por isso, seus códigos são rudimentares, pouco estruturados e pouco comentados. Em trabalhos futuros recomenda-se a estruturação, comentário e verificação desses códigos. Também foram empregados dispositivos de baixo custo como o NodeMCU. Embora esse dispositivo seja prático do ponto de vista da implementação de redes Wi-Fi, sua capacidade de processamento é demasiadamente limitada. Sugere-se o emprego de sistemas microcontrolados ou até microprocessados em lugar desses dispositivos o que possibilitaria até mesmo a implementação do protocolo Modbus TCP/IP nos próprios nós, eliminando a necessidade de um Gateway.

É visível também que a rede mesh montada para o trabalho não possui nenhuma estrutura de aprendizado. Melhorias nesse sentido são muito bem vindas para permitir o crescimento facilitado da rede e reorganização geográfica prática.

Um vídeo demonstrativo deste projeto foi produzido e está disponível [online](https://www.youtube.com/watch?v=lNfR3H1qQ2Q&t=58s). 

Toda contribuição é bem vinda.
