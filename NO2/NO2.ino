#include <ESP8266WiFi.h>
#include <ESP8266WiFiAP.h>
#include <ESP8266WiFiGeneric.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WiFiScan.h>
#include <ESP8266WiFiSTA.h>
#include <ESP8266WiFiType.h>
#include <WiFiClient.h>
#include <WiFiClientSecure.h>
#include <WiFiServer.h>
#include <WiFiUdp.h>

#define pin_led 2

WiFiServer server(80);

char* myssid = "node2";
char* mypass = "12345678";
char* ssidap = "node3";
char* passap = "12345678";

IPAddress ip(192,168,11,4);
IPAddress gateway(192,168,11,1);
IPAddress subnet(255,255,255,0);

String request = "";

IPAddress serverap(192,168,12,4);       // the fix IP address of the server
WiFiClient client;

void setup() {
  // put your setup code here, to run once:
  pinMode(pin_led, OUTPUT);
  digitalWrite(pin_led, LOW);
  
  Serial.begin(115200);

  WiFi.mode(WIFI_AP_STA);

  WiFi.softAPConfig(ip, gateway, subnet);
  WiFi.softAP(myssid, mypass);

  WiFi.begin(ssidap,passap);
  
  while(WiFi.status()!=WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }
  Serial.println("CONNECTED");   

  server.begin();   

}

void loop() {
  WiFiClient client = server.available();
  if (client) {
    if (client.connected()) {
      Serial.println(".");
      request = client.readStringUntil('\r');    // receives the message from the client
      
      Serial.print("From client: "); Serial.println(request);
      client.flush();
      client.println("ok\r");
    }
    client.stop();                // tarminates the connection with the client
  }
  if (request=="no2toggle")
  {
    digitalWrite(pin_led,HIGH);
    request="";
  }
  if (request=="no2untoggle")
  {
    digitalWrite(pin_led,LOW);
    request="";
  }
  if (request=="no3toggle")
  {
    Serial.print("Sending to node 3: "); Serial.println(request);
    client.connect(serverap, 81);   // Connection to the server
    Serial.println(".");
    client.println(request+"\r");  // sends the message to the server
    String answer = client.readStringUntil('\r');   // receives the answer from the sever
    Serial.println("from server: " + answer);
    client.flush();
    request="";
    delay(100);     
  }  
  if (request=="no3untoggle")
  {
    Serial.print("Sending to node 3: "); Serial.println(request);
    client.connect(serverap, 81);   // Connection to the server
    Serial.println(".");
    client.println(request+"\r");  // sends the message to the server
    String answer = client.readStringUntil('\r');   // receives the answer from the sever
    Serial.println("from server: " + answer);
    client.flush();
    request="";
    delay(100);     
  }
  
}
